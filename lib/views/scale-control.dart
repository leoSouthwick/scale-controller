import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import '../components/widgets.dart';

class ScaleControl extends StatefulWidget {
    ScaleControl({Key key, this.title}) : super(key: key);

// This widget is the home page of your application. It is stateful, meaning
// that it has a State object (defined below) that contains fields that affect
// how it looks.

// This class is the configuration for the state. It holds the values (in this
// case the title) provided by the parent (in this case the App widget) and
// used by the build method of the State. Fields in a Widget subclass are
// always marked "final".

    final String title;

    @override
    ScaleControlState createState() => ScaleControlState();
}

class ScaleControlState extends State<ScaleControl> {

    FlutterBlue _flutterBlue = FlutterBlue.instance;

    /// Scanning
    StreamSubscription _scanSubscription;
    Map<DeviceIdentifier, ScanResult> scanResults = new Map();
    bool isScanning = false;

    /// State
    StreamSubscription _stateSubscription;
    BluetoothState state = BluetoothState.unknown;

    /// Device
    BluetoothDevice device;
    bool get isConnected => (device != null);
    StreamSubscription deviceConnection;
    StreamSubscription deviceStateSubscription;
    List<BluetoothService> services = new List();
    Map<Guid, StreamSubscription> valueChangedSubscriptions = {};
    BluetoothDeviceState deviceState = BluetoothDeviceState.disconnected;

	BluetoothService serialService;
	BluetoothCharacteristic serialCharacteristic;

	int goalValue = 0;
	int toleranceValue = 1;

    @override
    void initState() {
        super.initState();
        // Immediately get the state of FlutterBlue
        _flutterBlue.state.then((s) {
            setState(() {
                state = s;
            });
        });
        // Subscribe to state changes
        _stateSubscription = _flutterBlue.onStateChanged().listen((s) {
            setState(() {
                state = s;
            });
        });

		_startScan();
    }

    @override
    void dispose() {
        _stateSubscription?.cancel();
        _stateSubscription = null;
        _scanSubscription?.cancel();
        _scanSubscription = null;
        deviceConnection?.cancel();
        deviceConnection = null;
        super.dispose();
    }

    _startScan() {
        _scanSubscription = _flutterBlue.scan(
            timeout: const Duration(seconds: 5),
            /*withServices: [
                    new Guid('0000180F-0000-1000-8000-00805F9B34FB')
                ]*/
        )
        .listen((scanResult) {
            if (scanResult.device.name == "SimpleScale") {
				print('localName: ${scanResult.advertisementData.localName}');
				print('manufacturerData: ${scanResult.advertisementData.manufacturerData}');
				print('serviceData: ${scanResult.advertisementData.serviceData}');
                _connect(scanResult.device);
				_stopScan();
            }
        }, onDone: _stopScan);

        setState(() {
            isScanning = true;
        });
    }

    _stopScan() {
        _scanSubscription?.cancel();
        _scanSubscription = null;
        setState(() {
            isScanning = false;
        });
    }

    _connect(BluetoothDevice d) async {
        device = d;
        // Connect to device
        deviceConnection = _flutterBlue
                .connect(device, timeout: const Duration(seconds: 4))
                .listen(
                    null,
                    onDone: _disconnect,
                );

        // Update the connection state immediately
        device.state.then((s) {
            setState(() {
                deviceState = s;
            });
        });

        // Subscribe to connection changes
        deviceStateSubscription = device.onStateChanged().listen((s) {
            setState(() {
                deviceState = s;
            });
            if (s == BluetoothDeviceState.connected) {
                device.discoverServices().then((s) {
                    setState(() {
                        services = s;
                    });
					print("Services: \n");
					s.forEach(_findSerial);
                });
            }
        });
    }

	_findSerial(BluetoothService service) {
		if(service.uuid.toString().toUpperCase().substring(4, 8) == "FFE0") {
			serialService = service;
			for(BluetoothCharacteristic characteristic in service.characteristics) {
				if(characteristic.uuid.toString().toUpperCase().substring(4, 8) == "FFE1") {
					serialCharacteristic = characteristic;
				}
			}
		}
		if(serialService != null && serialCharacteristic != null) {
			print("Found supported BLE module on device");
		}
	}

    _disconnect() {
        // Remove all value changed listeners
        valueChangedSubscriptions.forEach((uuid, sub) => sub.cancel());
        valueChangedSubscriptions.clear();
        deviceStateSubscription?.cancel();
        deviceStateSubscription = null;
        deviceConnection?.cancel();
        deviceConnection = null;
        setState(() {
            device = null;
        });
    }

    _readCharacteristic(BluetoothCharacteristic c) async {
        await device.readCharacteristic(c);
        setState(() {});
    }

    _writeCharacteristic(BluetoothCharacteristic c, String string) async {
        var action = utf8.encode(string);
        await device.writeCharacteristic(c, action,
                type: CharacteristicWriteType.withoutResponse);
        setState(() {});
    }

	sendCommand(String string) async {
		if(serialCharacteristic == null) { 
			print("No valid characteristic on bluetooth module");
			return; 
		}
		print("Sending Command " + string);
		await device.writeCharacteristic(serialCharacteristic, utf8.encode(string), type: CharacteristicWriteType.withoutResponse);
		setState(() {});
	}

    // _setNotification(BluetoothCharacteristic c) async {
    //     if (c.isNotifying) {
    //         await device.setNotifyValue(c, false);
    //         // Cancel subscription
    //         valueChangedSubscriptions[c.uuid]?.cancel();
    //         valueChangedSubscriptions.remove(c.uuid);
    //     } else {
    //         await device.setNotifyValue(c, true);
    //         // ignore: cancel_subscriptions
    //         final sub = device.onValueChanged(c).listen((d) {
    //             setState(() {
    //                 print('onValueChanged $d');
    //             });
    //         });
    //         // Add to map
    //         valueChangedSubscriptions[c.uuid] = sub;
    //     }
    //     setState(() {});
    // }

    _refreshDeviceState(BluetoothDevice d) async {
        var state = await d.state;
        setState(() {
            deviceState = state;
            print('State refreshed: $deviceState');
        });
    }

    _buildScanningButton() {
        if (isConnected || state != BluetoothState.on) {
            return null;
        }
        if (isScanning) {
            return new FloatingActionButton(
                child: new Icon(Icons.stop),
                onPressed: _stopScan,
                backgroundColor: Colors.red,
            );
        } else {
            return new FloatingActionButton(
                    child: new Icon(Icons.search), onPressed: _startScan);
        }
    }

    _buildScanResultTiles() {
        return scanResults.values
                .map((r) => ScanResultTile(
                            result: r,
                            onTap: () => _connect(r.device),
                        ))
                .toList();
    }

    List<Widget> _buildServiceTiles() {
        return services
			.map(
				(s) => new ServiceTile(
					service: s,
					characteristicTiles: s.characteristics.map(
						(c) => new CharacteristicTile(
							characteristic: c,
							onReadPressed: () => _readCharacteristic(c),
							onWritePressed: (String s) => _writeCharacteristic(c, s),
							// onNotificationPressed: () => _setNotification(c),
							descriptorTiles: []
						),
					).toList(),
				),
			).toList();
    }

    _buildActionButtons() {
        if (isConnected) {
            return <Widget>[
                new IconButton(
                    icon: const Icon(Icons.cancel),
                    onPressed: () => _disconnect(),
                )
            ];
        }
    }

    _buildAlertTile() {
        return new Container(
            color: Colors.redAccent,
            child: new ListTile(
                title: new Text(
                    'Bluetooth adapter is ${state.toString().substring(15)}',
                    style: Theme.of(context).primaryTextTheme.subhead,
                ),
                trailing: new Icon(
                    Icons.error,
                    color: Theme.of(context).primaryTextTheme.subhead.color,
                ),
            ),
        );
    }

    _buildDeviceStateTile() {
        return new ListTile(
                leading: (deviceState == BluetoothDeviceState.connected)
                        ? const Icon(Icons.bluetooth_connected)
                        : const Icon(Icons.bluetooth_disabled),
                title: new Text('Device is ${deviceState.toString().split('.')[1]}.'),
                subtitle: new Text('${device.id}'),
                trailing: new IconButton(
                    icon: const Icon(Icons.refresh),
                    onPressed: () => _refreshDeviceState(device),
                    color: Theme.of(context).iconTheme.color.withOpacity(0.5),
                ));
    }

    _buildProgressBarTile() {
        return new LinearProgressIndicator();
    }

    @override
    Widget build(BuildContext context) {
        var tiles = new List<Widget>();
		var title = "Scale Controller";
        if (state != BluetoothState.on) {
            tiles.add(_buildAlertTile());
        }
        if (isConnected) {
			title = device.name;
            tiles.add(_buildDeviceStateTile());
            tiles.addAll(_buildServiceTiles());
        } else {
			_startScan();
            //tiles.addAll(_buildScanResultTiles());
        }
        return new MaterialApp(
            home: new Scaffold(
                appBar: new AppBar(
                    title: Text(title),
                    actions: _buildActionButtons(),
                ),
                floatingActionButton: _buildScanningButton(),
                body: new Stack(
                    children: <Widget>[
                        (isScanning) ? _buildProgressBarTile() : new Container(),
						new Column(
							mainAxisSize: MainAxisSize.max, 
							mainAxisAlignment: MainAxisAlignment.spaceAround,
							children: <Widget>[
								new Container(
									//color: Colors.lightBlue[100],
									child: new FlatButton(
										child: new Row(
											mainAxisSize: MainAxisSize.max,
											mainAxisAlignment: MainAxisAlignment.center,
											children: <Widget>[
												const Text("Calibrate"),
												const Icon(Icons.rotate_left, color: Colors.blueAccent),
											],
										),
										onPressed: () => { sendCommand("calibrate") }
									),
								),
								new Container(
									//color: Colors.lightGreen,
									child: new Row(
										mainAxisAlignment: MainAxisAlignment.center,
										children: <Widget>[
											new Text("Target"),
											new Flexible(
												child: new TextField(
													onChanged:(String amount) => { goalValue = int.parse(amount) },
													keyboardType: TextInputType.number,
													textAlign: TextAlign.center,
												),
											),
											new Container(
												//color: Colors.grey,
												child: new IconButton(
													color: Colors.green,
													icon: new Icon(Icons.adjust),
													onPressed: () => { sendCommand("goal_" + goalValue.toString()) },
												),
											),
										],
									)
								),
								new Container(
									//color: Colors.lightGreen,
									child: new Row(
										mainAxisAlignment: MainAxisAlignment.center,
										children: <Widget>[
											new Text("Tolerance"),
											new Flexible(
												child: new TextField(
													onChanged:(String amount) => { toleranceValue = int.parse(amount) },
													keyboardType: TextInputType.number,
													textAlign: TextAlign.center,
												),
											),
											new Container(
												//color: Colors.grey,
												child: new IconButton(
													color: Colors.green,
													icon: new Icon(Icons.adjust),
													onPressed: () => { sendCommand("tolerance_" + toleranceValue.toString()) },
												),
											),
										],
									)
								),
							],
						),
                    ],
                ),
            ),
        );
    }
}
